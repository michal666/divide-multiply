let SCORE;

let add = (number_1, number_2) => {
  SCORE = parseInt(number_1) + parseInt(number_2);
  return SCORE;
};

let multiply = () => {
  SCORE *= 2;
  return SCORE;
};
let divide = () => {
  SCORE /= 2;
  return SCORE;
};

const form = document.querySelector("#form");
const oryginaly = document.getElementById("oryginaly");

let clearInput = () => {
  document.getElementById("firstNumber").value = "";
  document.getElementById("secoundNumber").value = "";
};
let clearNewResult = () => {
  document.getElementById("newResult").innerHTML = "";
};

form.addEventListener("click", (e) => {
  if (e.target.matches("#resultButton")) {
    clearNewResult();
    let first_number = document.getElementById("firstNumber").value;
    let secound_number = document.getElementById("secoundNumber").value;
    oryginaly.innerHTML = add(first_number, secound_number);
    clearInput();
  }
});

const container = document.querySelector("#calculations");
const newResult = document.getElementById("newResult");

container.addEventListener("click", (e) => {
  if (newResult.innerHTML === "") {
    if (e.target.matches("#multiply")) {
      newResult.innerHTML = multiply();
    } else if (e.target.matches("#divide")) {
      newResult.innerHTML = divide();
    }
  }
});

// var multiplyButton = document.getElementById("multiply");
// var divideButton = document.getElementById("divide");

// multiplyButton.addEventListener("click", multiply);
// divideButton.addEventListener("click", divide);

// console.log(SCORE);
